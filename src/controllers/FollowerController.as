
package controllers
{
	import elements.Follower;
	import elements.TargetElement;
	
	import events.FollowerControllerEvent;
	import events.FollowerEvent;
	
	import flash.events.EventDispatcher;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Apr 7, 2012
	 */
	public class FollowerController extends EventDispatcher
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		protected var _followers:Vector.<Follower>;
		private var _activeFollowers:Vector.<Follower>;
		private var _idleFollowers:Vector.<Follower>;
		private var _inactiveFollowers:Vector.<Follower>;
		
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function FollowerController()
		{
			_followers = new <Follower>[];
			_activeFollowers = new <Follower>[];
			_idleFollowers = new <Follower>[];
			_inactiveFollowers = new <Follower>[];
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		private function foundTarget(e:FollowerEvent):void
		{
			if(e.follower.currentTarget == null && _activeFollowers.indexOf(e.follower) > -1)
			{
				_activeFollowers.splice(_activeFollowers.indexOf(e.follower), 1);
				_idleFollowers.push(e.follower);
			}
		}
		
		protected function deactivateFollower(follower:Follower):void
		{
			_inactiveFollowers.push(follower);
			if(_activeFollowers.indexOf(follower) != -1)
				_activeFollowers.splice(_activeFollowers.indexOf(follower), 1);
			
			if(_idleFollowers.indexOf(follower) != -1)
				_idleFollowers.splice(_idleFollowers.indexOf(follower), 1);
			
			if(_activeFollowers.length == 0 && _idleFollowers.length == 0)
				dispatchEvent(new FollowerControllerEvent(FollowerControllerEvent.ALL_FOLLOWER_INACTIVE));
		}
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public function addFollower(follower:Follower):void
		{
			_idleFollowers.push(follower);
			if(_followers.indexOf(follower) == -1)
			{
				_followers.push(follower);
				follower.addEventListener(FollowerEvent.FOUND_TARGET, foundTarget);
			}
		}
		
		public function moveFollowers(idleTargets:Vector.<TargetElement>):void
		{
			//			trace("moveFollowers");
			var follower:Follower;
			for each(follower in _activeFollowers)
			{
				if(!follower.move())//follower moved
					_activeFollowers.splice(_activeFollowers.indexOf(follower), 1);
			}
			
			for each(follower in _idleFollowers)
			{
				if(follower.currentTarget == null)
				{
					var rand:uint = Math.floor(Math.random() * idleTargets.length);
					var newTarget:TargetElement = idleTargets[rand];
					
					if(newTarget != follower.currentTarget)
					{
						follower.currentTarget = newTarget;
					}
				}
				if(!follower.move())//follower moved
					_idleFollowers.splice(_idleFollowers.indexOf(follower), 1);
			}
		}
		
		public function requestFollower(newTarget:TargetElement):Boolean
		{
			if(_idleFollowers.length > 0)
			{
				for each(var follower:Follower in _idleFollowers)
				{
					if(follower.rested)
					{
						follower.currentTarget = newTarget;
						_activeFollowers.push(follower);
						_idleFollowers.splice(_idleFollowers.indexOf(follower), 1);
						return true;
					}
				}
			}
			trace('I found no followers');
			return false;
		}
		
		public function rebirth():void
		{
			_activeFollowers.length = 0;
			_inactiveFollowers.length = 0;
			_idleFollowers.length = 0;
			var follower:Follower;
			for each(follower in _followers)
			{
				follower.rebirth();
				addFollower(follower);
			}
		}
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}

package data
{
	import com.ghelton.WatchTower;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 10, 2012
	 */
	public class Environment
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		public static const DAY_TIME:uint = 500;
		public static const DAYS:uint = 365;
		public static const LAST_DAY:uint = DAYS - 1;
		public static const SEASON_TEMPS:Vector.<Number> = new <Number>[0, 10, 0, -10];
		public static const DAYS_IN_SEASON:Number = DAYS / SEASON_TEMPS.length;
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		private var _currentDay:uint = 0;
		private var _currentSeason:uint = 0;
		private var _currentSeasonProgress:Number = 0;
		private var _previousSeason:uint = SEASON_TEMPS.length - 1;
		private var _timeTillNextDay:uint = 0;
		public var currentTime:Number = 0;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Environment()
		{
			WatchTower.addWatcher(new Watcher(DAY_TIME, dayTick));
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		private function newDay():void
		{
			//update times
			if(_currentDay < LAST_DAY)
				_currentDay++;
			else
				_currentDay = 0;
			_currentSeasonProgress = (_currentDay % DAYS_IN_SEASON) / DAYS_IN_SEASON;
			var newSeason:uint = Math.floor(_currentDay / DAYS_IN_SEASON);
			if(newSeason != _currentSeason)
			{
				_previousSeason = _currentSeason;
				_currentSeason = newSeason;
			}
		}
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public function dayTick(dt:uint):void
		{
			if(_timeTillNextDay == 0 || dt > _timeTillNextDay) {
				newDay();
				_timeTillNextDay = DAY_TIME;
				currentTime = 0;
			} else {
				_timeTillNextDay -= dt;
				currentTime = _timeTillNextDay / DAY_TIME;
			}
		}
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}
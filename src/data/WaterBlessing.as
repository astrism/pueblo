
package data
{
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class WaterBlessing extends Blessing
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		private static const WATER_COLOR:uint = 0xAAAAFF;
		private static const GLOW:GlowFilter = new GlowFilter(0x0000FF, 1, 5, 5, 1, 3);
		private static const WATER_FILTERS:Array = [GLOW];
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function WaterBlessing()
		{
			super();
			filters = WATER_FILTERS;
			color = WATER_COLOR;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}
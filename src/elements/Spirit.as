
package elements
{
	import com.ghelton.WatchTower;
	
	import data.Blessing;
	import data.Burden;
	import data.Environment;
	import data.Watcher;
	
	import events.SpiritEvent;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 10, 2012
	 */
	public class Spirit extends Follower
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		private static const FRAME_WALK:String = "walk";
		private static const FRAME_CARRY:String = "carry";
		private static const FRAME_BLESSED:String = "blessed";
		private static const FRAME_FIGHT:String = "fight";
		private static const FRAME_TIRED:String = "tired";
		private static const FRAME_DEATH:String = "death";
		
		protected static const CHILD_LOADPOINT:String = "loadpoint";
		
		private static const DEFAULT_COLOR:uint = 0xFFFFFF;
		private static const INIT_STRENGTH:uint = 25;
		private static const INIT_HEALTH:uint = 100;
		private static const MAX_SPEED:uint = 500 / Environment.DAY_TIME; //spirit move slower if the day is longer
		private static const CONSUMPTION:uint = 1;
		private static const HUNGER_THRESHOLD:uint = 22;
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		public var strength:uint = INIT_STRENGTH;
		public var health:uint = INIT_HEALTH;
		protected var _burden:Burden;
		protected var _blessing:Blessing;
		public var hunger:uint = 0;
		private var _presentation:MovieClip;
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Spirit($birthplace:TargetElement, $presentation:MovieClip)
		{
			super($birthplace);
			_presentation = $presentation;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------
		override protected function init(e:Event):void
		{
			super.init(e);
			addChild(_presentation);
			WatchTower.addWatcher(new Watcher(Environment.DAY_TIME, dayTick));
			WatchTower.addWatcher(new Watcher(Environment.DAY_TIME * 7, monthTick));
		}
		
		override protected function draw():void
		{
			super.draw();
			if(_blessing != null)
			{
				_presentation.gotoAndPlay(FRAME_BLESSED);
			} else if (_burden != null) {
				_presentation.gotoAndPlay(FRAME_CARRY);
				if(_presentation[CHILD_LOADPOINT] != null)
				{
					_presentation[CHILD_LOADPOINT].addChild(_burden.presentation);
					_presentation.
					_burden.presentation.gotoAndStop(Burden.FRAME_END);
					_burden.presentation.scaleX = _burden.presentation.scaleY = 2;
				}
			} else {
				if(_presentation[CHILD_LOADPOINT] != null)
				{
					while(_presentation[CHILD_LOADPOINT].numChildren > 0)
						_presentation[CHILD_LOADPOINT].removeChildAt(0);
				}
				if(_presentation.currentFrameLabel != FRAME_WALK)
				{
					_presentation.gotoAndPlay(FRAME_WALK);
				}
			}
			this.cacheAsBitmap = true;
		}
		
		override protected function foundTarget():void
		{
			if(currentTarget is Repository)
			{
				var repo:Repository = currentTarget as Repository;
				if(_burden != null)
					repo.drop(_burden);
				_burden = null;
				_blessing = null;
				repo.consume(this);//return is the remainder that the repository did not have available
//				trace("hunger:", hunger);
			} else if(currentTarget is Kiva) {
				_blessing = (currentTarget as Kiva).blessing;
				_burden = null;
			} else if(currentTarget is Resource) {
				_burden = (currentTarget as Resource).getBurden(strength);
				_blessing = null;
			} else {
				_burden = null;
				_blessing = null;
			}
			redraw();
				
			super.foundTarget();
		}
		
		override public function get rested():Boolean
		{
			var isRested:Boolean = hunger < 30;
			if(isRested)
				_presentation.gotoAndPlay(FRAME_WALK);
			else				
				_presentation.gotoAndPlay(FRAME_TIRED);
			return isRested;
		}
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public function monthTick(dt:uint):void
		{
			if(health < INIT_HEALTH && hunger < 10)
				health++;
		}
		public function dayTick(dt:uint):void
		{
			hunger += CONSUMPTION;
			if(hunger > HUNGER_THRESHOLD)
			{
				if(health > 0)
				{
					health--;
				} else {
					WatchTower.pauseWatcher(Environment.DAY_TIME, dayTick);
					dispatchEvent(new SpiritEvent(SpiritEvent.DEATH));
					_presentation.gotoAndPlay(FRAME_DEATH);
				}
			}
		}
		
		override public function rebirth():void
		{
			_burden = null;
			_blessing = null;
			health = INIT_HEALTH;
			strength = INIT_STRENGTH;
			hunger = 0;
			WatchTower.resumeWatcher(Environment.DAY_TIME, dayTick);
			super.rebirth();
		}
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------	
	}
}

package elements
{
	import com.ghelton.WatchTower;
	
	import data.Watcher;
	
	import fl.text.TLFTextField;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since May 6, 2012
	 */
	public class HeroSpirit extends Spirit
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		private var _hero:Hero;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function HeroSpirit($birthplace:TargetElement, $presentation:MovieClip)
		{
			super($birthplace, $presentation);
			_hero = $presentation as Hero;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------
		override protected function draw():void
		{
			super.draw();
			if (_blessing != null) {
				var loadpoint:MovieClip = _hero.thoughtBubble[Spirit.CHILD_LOADPOINT] as MovieClip;
				if(loadpoint.numChildren == 0)
					loadpoint.addChild(_burden.presentation);
			}
		}
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------
	}
}
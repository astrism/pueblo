
package elements
{
	import data.Burden;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class Repository extends TargetElement
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		public static const REPOSITORY_WATER:uint = 0;
		public static const REPOSITORY_FOOD:uint = 1;
		
		private static const COLOR_RED:uint = 0x712215;
		private static const COLOR_BLUE:uint = 0x032B5E;
		private static const COLOR_GREEN:uint = 0x27912F;
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		private static var _repos:Vector.<Repository> = new <Repository>[];
		private var _value:uint;
		private var _maxValue:uint;
		private var _type:uint;
		private var _presentation:MovieClip;
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Repository($presentation:MovieClip, $repoType:uint, $initValue:uint)
		{
			super($presentation);
			_presentation = $presentation;
			_type = $repoType;
			_value = _maxValue = $initValue;
			_repos.push(this);
			
			var frame:uint = (_value / _maxValue) * _presentation.totalFrames;
			_presentation.gotoAndStop(frame);
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	

		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public static function findRepo($repoType:uint):void
		{
			
		}
		
		public function consume(spirit:Spirit):uint
		{
			if(_value == 0)
				return 0;
			var remainder:uint;
			var hunger:uint = Math.floor(spirit.hunger);
			if(hunger > _value)
			{
				remainder = hunger % _value;
				_value = 0;
			} else {
				_value -= hunger;
				remainder = 0;
			}
			spirit.hunger = remainder;
			
			var frame:uint = (_value / _maxValue) * _presentation.totalFrames;
			_presentation.gotoAndStop(frame);
				
			return remainder;
		}
		
		public function drop(burden:Burden):void
		{
			if(_value + burden.value < _maxValue)
				_value += burden.value;
			else
				_value = _maxValue;
		}
		
		public function get value():uint
		{
			return _value;
		}
		
		public function get type():uint
		{
			return _type;
		}
		
		public function rebirth():void
		{
			_value = _maxValue;
			var frame:uint = (_value / _maxValue) * _presentation.totalFrames;
			_presentation.gotoAndStop(frame);
		}
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}
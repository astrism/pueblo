
package elements
{
	import data.Blessing;
	
	import flash.display.DisplayObject;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 17, 2012
	 */
	public class Kiva extends TargetElement
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		private var _resource:TargetElement;
		public var blessing:Blessing;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Kiva($presentation:DisplayObject, resource:TargetElement, $blessing:Blessing)
		{
			super($presentation);
			_resource = resource;
			blessing = $blessing;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		override public function getNextTarget():TargetElement
		{
			if(_resource)
			{
				return _resource;
			} else
				return super.getNextTarget();
		}
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}